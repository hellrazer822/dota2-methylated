#pragma once

#include "memory.h"
#include "d3d9.h"
#include "vmt.h"
#include "framestagenotify.h"
#include "interfaces.h"

namespace Hooks
{
	extern VMT* entityVMT;
	extern VMT* panelVMT;
	extern VMT* particleMgrVMT;
	extern VMT* particleCollectionVMT;
	extern VMT* clientVMT;

	void Init();
	void Shutdown();
}