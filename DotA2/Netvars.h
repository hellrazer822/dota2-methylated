#pragma once
#include <unordered_map>
#include <string>
#include <cstdint>

#include "interfaces.h"

#include "simplesdk/CSource2Client.h"

namespace Netvars
{
    void DumpNetvars( CSource2Client *clientInterface, const char *fileName );
    void CacheNetvars( CSource2Client *client );

    extern std::unordered_map<std::string, std::unordered_map<std::string, uint32_t>> netvars;
}