#include "interfaces.h"

#include "console.h"


 CGlobalVars* globalVars;
 IVPanel* panel;
 CSource2Client* client;
 ICvar* cvar;
 IEngineClient* engine;
 IInputInternal* inputInternal;
 IInputSystem* inputSystem;
 INetworkClientService* networkClientService;
 CSplitScreenService* splitScreenService;
 CGameEntitySystem* entitySystem;
 CVScriptGameSystem* vscriptSystem;
 IPanoramaUIEngine* panoramaEngine;
 CNetworkMessages* networkMessages;
 CViewRender* viewRender;
 IClientMode* clientMode;
 CDotaCamera* camera;
 CGameEventManager* gameEventManager;
 CSoundOpSystem* soundOpSystem;
 CFontManager* fontManager;
 CEngineServiceMgr* engineServiceMgr;
 CDOTARichPresence* richPresence;
 CParticleSystemMgr* particleSystemMgr;
 CGameEventSystem* gameEventSystem;
 CNetworkStringTableContainer* networkStrings;
 IMaterialSystem* materialSystem;
 CVPhys2World* phys2World;
 CRenderGameSystem* renderGameSystem;
 CNetworkSystem* networkSystem;
 CGCClient* gcClient;
 CBaseFileSystem* fileSystem; // there is another pointer to this in Logger


namespace Interfaces
{
	void* GetInterface(const char* dllname, const char* interfacename)
	{
		CreateInterfaceFn CreateInterface = reinterpret_cast<CreateInterfaceFn>(GetProcAddress(GetModuleHandleA(dllname), "CreateInterface"));

		int returnCode = 0;
		void* interfaceses = CreateInterface(interfacename, &returnCode);

		return interfaceses;
	}

	void CreateInterfaces()
	{
		client = reinterpret_cast<CSource2Client*>(GetInterface("client.dll", "Source2Client002"));
		cvar = reinterpret_cast<ICvar*>(GetInterface("tier0.dll", "VEngineCvar007"));
		engine = reinterpret_cast<IEngineClient*>(GetInterface("engine2.dll", "Source2EngineToClient001"));
		inputSystem = reinterpret_cast<IInputSystem*>(GetInterface("inputsystem.dll", "InputSystemVersion001"));
		inputInternal = reinterpret_cast<IInputInternal*>(GetInterface("vgui2.dll", "VGUI_InputInternal001"));
		networkClientService = reinterpret_cast<INetworkClientService*>(GetInterface("engine2.dll", "NetworkClientService_001"));
		panel = reinterpret_cast<IVPanel*>(GetInterface("vgui2.dll", "VGUI_Panel010"));
		splitScreenService = reinterpret_cast<CSplitScreenService*>(GetInterface("engine2.dll", "SplitScreenService_001"));
		panoramaEngine = reinterpret_cast<IPanoramaUIEngine*>(GetInterface("panorama.dll", "PanoramaUIEngine001"));
		fontManager = reinterpret_cast<CFontManager*>(GetInterface("materialsystem2.dll", "FontManager_001"));
		engineServiceMgr = reinterpret_cast<CEngineServiceMgr*>(GetInterface("engine2.dll", "EngineServiceMgr001"));
		particleSystemMgr = reinterpret_cast<CParticleSystemMgr*>(GetInterface("particles.dll", "ParticleSystemMgr003"));
		networkMessages = reinterpret_cast<CNetworkMessages*>(GetInterface("networksystem.dll", "NetworkMessagesVersion001"));
		gameEventSystem = reinterpret_cast<CGameEventSystem*>(GetInterface("engine2.dll", "GameEventSystemClientV001"));
		networkStrings = reinterpret_cast<CNetworkStringTableContainer*>(GetInterface("engine2.dll", "Source2EngineToClientStringTable001"));
		materialSystem = reinterpret_cast<IMaterialSystem*>(GetInterface("materialsystem2.dll", "VMaterialSystem2_001"));
		networkSystem = reinterpret_cast<CNetworkSystem*>(GetInterface("networksystem.dll", "NetworkSystemVersion001"));
		fileSystem = reinterpret_cast<CBaseFileSystem*>(GetInterface("filesystem_stdio.dll", "VFileSystem017"));
	}

	void Dump()
	{
		#define STRINGIFY_IMPL(s) #s
		#define STRINGIFY(s)      STRINGIFY_IMPL(s)
		#define PRINT_INTERFACE(name) g_console.log("%-30s: %p", STRINGIFY(name), name)

		g_console.log("-=-=-=-=-=-=-=-=-= Interfaces =-=-=-=-=-=-=-=-=-");

		PRINT_INTERFACE(client);
		PRINT_INTERFACE(cvar);
		PRINT_INTERFACE(engine);
		PRINT_INTERFACE(panel);
		PRINT_INTERFACE(inputSystem);
		PRINT_INTERFACE(inputInternal);
		PRINT_INTERFACE(networkClientService);
		PRINT_INTERFACE(splitScreenService);
		PRINT_INTERFACE(panoramaEngine);
		PRINT_INTERFACE(fontManager);
		PRINT_INTERFACE(engineServiceMgr);
		PRINT_INTERFACE(networkMessages);
		PRINT_INTERFACE(gameEventSystem);
		PRINT_INTERFACE(networkStrings);
		PRINT_INTERFACE(materialSystem);
		PRINT_INTERFACE(networkSystem);
		PRINT_INTERFACE(fileSystem);
	}
}