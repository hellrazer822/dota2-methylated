#pragma once

#include <windows.h>
#include <string>
#include <WinUser.h>

using namespace std;

extern std::string username;
extern std::string password;
extern std::string pcid;
extern std::string subscription;
extern std::string group;
extern std::string configautorun;
extern std::string skinautorun;

void IniFileED();
int InitPath(const char* szPath);
int LoadCvar(char* szSection, char* szKey, int DefaultValue);
string LoadCvar(char* szSection, char* szKey, string DefaultValue);
float LoadCvar(char* szSection, char* szKey, float DefaultValue);
void SaveCvar(char* szSection, char* szKey, int Value);
void SaveCvar(char* szSection, char* szKey, float Value);
void SaveCvar(char* szSection, char* szKey, string Value);
void FetchUserData();