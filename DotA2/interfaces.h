#pragma once

#include <windows.h>
#include "simplesdk/sdk.h"
#include "vmt.h"
#include <vector>

extern CGlobalVars* globalVars;
extern IVPanel* panel;
extern CSource2Client* client;
extern ICvar* cvar;
extern IEngineClient* engine;
extern IInputInternal* inputInternal;
extern IInputSystem* inputSystem;
extern INetworkClientService* networkClientService;
extern CSplitScreenService* splitScreenService;
extern CGameEntitySystem* entitySystem;
extern CVScriptGameSystem* vscriptSystem;
extern IPanoramaUIEngine* panoramaEngine;
extern CNetworkMessages* networkMessages;
extern CViewRender* viewRender;
extern IClientMode* clientMode;
extern CDotaCamera* camera;
extern CGameEventManager* gameEventManager;
extern CSoundOpSystem* soundOpSystem;
extern CFontManager* fontManager;
extern CEngineServiceMgr* engineServiceMgr;
extern CDOTARichPresence* richPresence;
extern CParticleSystemMgr* particleSystemMgr;
extern CGameEventSystem* gameEventSystem;
extern CNetworkStringTableContainer* networkStrings;
extern IMaterialSystem* materialSystem;
extern CVPhys2World* phys2World;
extern CRenderGameSystem* renderGameSystem;
extern CNetworkSystem* networkSystem;
extern CGCClient* gcClient;
extern CBaseFileSystem* fileSystem; // there is another pointer to this in Logger

namespace Interfaces
{
	void* GetInterface(const char* dllname, const char* interfacename);
	void CreateInterfaces();
	void Dump();
}