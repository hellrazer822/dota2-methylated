#include <windows.h>
#include <chrono>
#include <thread>

#include "features.h"
#include "Hooks.h"
#include "console.h"
#include "Menu.h"

#include "interfaces.h"
#include "user.h"

void InitHack()
{
    g_console.allocate("Methylated.cc");

    CreateDirectoryA("C:\\Methylated\\dota2", NULL);
    CreateDirectoryA("C:\\Methylated\\dota2\\configs", NULL);

    g_console.enable_log_file("C:\\Methylated\\dota2\\startup.log");

    FetchUserData();

    Interfaces::CreateInterfaces();
    Interfaces::Dump();
    Hooks::Init();

    g_console.log("-=-=-=-=-=-=-=-=-=-= Other -=-=-=-=-=-=-=-=-=-=-");
}

DWORD WINAPI OnDllAttach(LPVOID base)
{
    InitHack();

    while (!Globals::Unload)
    {
        if (GetAsyncKeyState(VK_INSERT))
           Menu::Get().Switch();

        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }

    g_console.detach();
    Hooks::Shutdown();
    Menu::Get().Shutdown();

    FreeLibraryAndExitThread((HMODULE)base, 0);
    return false;
}

BOOL WINAPI DllMain(_In_ HINSTANCE hinstDll, _In_ DWORD fdwReason, _In_opt_ LPVOID lpvReserved)
{
    if (fdwReason == DLL_PROCESS_ATTACH)
        DisableThreadLibraryCalls(hinstDll),
        CreateThread(nullptr, NULL, OnDllAttach, hinstDll, NULL, nullptr);

    return TRUE;
}

