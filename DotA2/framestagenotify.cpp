#include "framestagenotify.h"

static const char* Stage2String(ClientFrameStage_t stage) {
    switch (stage) {
        CASE_STRING(FRAME_UNDEFINED);
        CASE_STRING(FRAME_START);
        CASE_STRING(FRAME_NET_UPDATE_START);
        CASE_STRING(FRAME_NET_UPDATE_POSTDATAUPDATE_START);
        CASE_STRING(FRAME_NET_UPDATE_POSTDATAUPDATE_END);
        CASE_STRING(FRAME_NET_UPDATE_END);
        CASE_STRING(FRAME_RENDER_START);
        CASE_STRING(FRAME_RENDER_END);
    default:
        return "gaaaa";
    }
}

void* FrameStageNotify(CSource2Client* thisptr, ClientFrameStage_t stage) 
{


    return Hooks::clientVMT->GetOriginalMethod(FrameStageNotify)(thisptr, stage);
}