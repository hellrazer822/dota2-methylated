#include "Render.h"
#include "menu.h"
#include "console.h"

void Draw::DrawInit(IDirect3DDevice9* pDevice)
{
	this->pDevice = pDevice;
	pDevice->GetViewport(&Viewport);

	Menu::Get().Init(pDevice);
	render_drawlist = ImGui::GetForegroundDrawList();
}

void Draw::AddText(const char* text, ImVec2 pos, ImColor color)
{
	render_drawlist->AddText(pos,color,text);
}

void Draw::AddLine(ImVec2 pos1, ImVec2 pos2, ImColor color, float thikness)
{
	render_drawlist->AddLine(pos1, pos2, color, thikness);
}

void Draw::AddRect(ImVec2 pos1, ImVec2 pos2, ImColor color, float rounding, float thikness)
{
	render_drawlist->AddRect(pos1, pos2, color, rounding,15,thikness);
}

void Draw::AddRectFilled(ImVec2 pos1, ImVec2 pos2, ImColor color, float rounding)
{
	render_drawlist->AddRectFilled(pos1, pos2, color, rounding);
}

void Draw::AddRectFilledMultiColor(ImVec2 pos1, ImVec2 pos2, ImColor color1, ImColor color2, ImColor color3, ImColor color4)
{
	render_drawlist->AddRectFilledMultiColor(pos1, pos2, color1, color2, color3, color4);
}

Draw g_Render;