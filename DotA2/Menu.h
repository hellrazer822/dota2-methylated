#pragma once

#include "Render.h"

#include "imgui/imgui.h"
#define IMGUI_DEFINE_MATH_OPERATORS
#include "imgui/imgui_internal.h"
#include "imgui/imgui_impl_dx9.h"
#include "imgui/imgui_impl_win32.h"
#include "singleton.hpp"

#include "options.h"

#include "customgui.h"
#include "sffont.hpp"

namespace Font
{
	extern ImFont* main;
	extern ImFont* main_middle;
	extern ImFont* main_big;
}

class Menu : public Singleton<Menu>
{
public:
	void Init(IDirect3DDevice9* pDevice);
	void Render();
	void NewFrame();
	void RenderFrame();
	void Shutdown();
	void Switch();
    bool IsVisible();
	void CreateStyle();
	void InitFonts();

	bool _visible = true;
	int  _selectedtab = 0;
	float _selectedtabsize = 30;
	ImVec2 _selectedpos;

	IDirect3DTexture9* avatar;
private:
	ImVec2 menu_pos;
	ImDrawList* menu_draw;
	int menu_flags = ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse;
};