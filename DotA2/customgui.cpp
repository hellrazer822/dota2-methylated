﻿#include "customgui.h"

#include <ctime>
#include <algorithm>

namespace CGUI
{
	void DrawLogo(ImVec2 center, float size, bool pulse, float speeed, int alpha) {
		ImDrawList* draw_list = ImGui::GetCurrentWindow()->DrawList;
		static float mult = 0;
		if (pulse) {
			static bool pulse_state = true;
			if (pulse_state) {
				mult += speeed * (60 / ImGui::GetIO().Framerate);
				if (mult >= 30.f)
					pulse_state = false;
			}
			else if (!pulse_state) {
				mult -= speeed * (60 / ImGui::GetIO().Framerate);
				if (mult <= 0.0f)
					pulse_state = true;
			}
		}

		ImColor light	= ImColor(141 - (int)mult, 143 - (int)mult, 142 - (int)mult, alpha),
				mid		= ImColor(107  - (int)mult, 107 - (int)mult, 107 - (int)mult, alpha),
				dark	= ImColor(82  - (int)mult, 82 - (int)mult, 82 - (int)mult, alpha);

		draw_list->AddTriangleFilled(center,
			{ center.x, center.y - size },
			{ center.x - float(size / 2.5), center.y - size / 5 },
			mid);
		draw_list->AddTriangleFilled(center,
			{ center.x, center.y - size },
			{ center.x + float(size / 2.5), center.y - size / 5 },
			light);
		draw_list->AddTriangleFilled({ center.x - float(size / 2), center.y },
			{ center.x - float(size * 1.2), center.y + float(size / 1.5) },
			{ center.x - size, center.y - float(size / 1.5) },
			mid);
		draw_list->AddTriangleFilled({ center.x + float(size / 2), center.y },
			{ center.x + float(size * 1.2), center.y + float(size / 1.5) },
			{ center.x + size, center.y - float(size / 1.5) },
			dark);
		draw_list->AddTriangleFilled({ center.x - float(size / 2), center.y },
			{ center.x - float(size * 1.2), center.y + float(size / 1.5) },
			{ center.x - size/4, center.y + float(size / 2.7) },
			light);
		draw_list->AddTriangleFilled({ center.x + float(size / 2), center.y },
			{ center.x + float(size * 1.2), center.y + float(size / 1.5) },
			{ center.x + size / 4, center.y + float(size / 2.7) },
			light);
		draw_list->AddTriangleFilled(center,
			{ center.x, center.y + float(size / 1.2) },
			{ center.x - size, center.y - float(size / 1.5) },
			dark);
		draw_list->AddTriangleFilled(center,
			{ center.x, center.y + float(size / 1.2) },
			{ center.x + size, center.y - float(size / 1.5) },
			mid);
	}

	bool Tab(const char* label, bool selected)
	{
		ImGuiWindow* window = ImGui::GetCurrentWindow();
		if (window->SkipItems)
			return false;

		ImGuiContext& g = *GImGui;
		const ImGuiStyle& style = g.Style;
		const ImGuiID id = window->GetID(label);
		const ImVec2 label_size = ImGui::CalcTextSize(label, NULL, true);

		ImVec2 pos = window->DC.CursorPos;
		ImVec2 size = ImGui::CalcItemSize({180,30}, label_size.x + style.FramePadding.x * 2.0f, label_size.y + style.FramePadding.y * 2.0f);

		const ImRect bb(pos, pos + size);
		ImGui::ItemSize(size, style.FramePadding.y);
		if (!ImGui::ItemAdd(bb, id))
			return false;

		bool hovered, held;
		bool pressed = ImGui::ButtonBehavior(bb, id, &hovered, &held, NULL);
		
		window->DrawList->AddRectFilled(bb.Min, bb.Min + ImVec2(5, 30), selected ? ImColor(66, 138, 254) : ImColor(120, 120, 120), 12);

		if (selected)
			window->DrawList->AddCircleFilled(bb.Max - ImVec2(10, 15), 3, ImColor(66, 138, 254), 32);

		window->DrawList->AddRectFilled(bb.Min, bb.Min + ImVec2(5, 30), selected ? ImColor(66, 138, 254) : ImColor(170, 170, 170), 12);
		window->DrawList->AddText(bb.Min + ImVec2(17, 7),ImColor(190, 190, 190),label);
		

		return pressed;
	}

}