#pragma once

#include "memory.h"

#include <d3d9.h>
#include <d3dx9.h>
#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")

#define WNDPROC_INDEX GWLP_WNDPROC

#define RELEASE_D3D 1

namespace D3D9
{
	typedef long(__stdcall* EndScene_t)(LPDIRECT3DDEVICE9);
	typedef LRESULT(__stdcall* WndProc_t)(const HWND, UINT, WPARAM, LPARAM);
	extern mem_t EndSceneAddress;
	extern EndScene_t oEndScene;
	extern WndProc_t oWndProc;
	extern HWND hWnd;
	extern void* vTable[119];

	bool CreateDummyDevice(void** vtable, size_t size);
	LRESULT HookWndProc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam);
	long __stdcall hkEndScene(LPDIRECT3DDEVICE9 pDevice);
}