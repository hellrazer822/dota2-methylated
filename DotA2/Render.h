#pragma once

#include "d3d9.h"

#include "imgui/imgui.h"
#include "imgui/imgui_internal.h"

class Draw
{
public:
	void DrawInit(IDirect3DDevice9* pDevice);
	IDirect3DDevice9* pDevice;

	// Draw List Render

	void AddText(const char* text, ImVec2 pos, ImColor color);
	void AddLine(ImVec2 pos1,ImVec2 pos2,ImColor color,float thikness);
	void AddRect(ImVec2 pos1, ImVec2 pos2, ImColor color,float rounding, float thikness);
	void AddRectFilled(ImVec2 pos1, ImVec2 pos2, ImColor color, float rounding);
	void AddRectFilledMultiColor(ImVec2 pos1, ImVec2 pos2, ImColor color1, ImColor color2, ImColor color3, ImColor color4);
private:
	ImDrawList* render_drawlist;

	D3DVIEWPORT9 Viewport;
};

extern Draw g_Render;