#include "Hooks.h"

#include "Menu.h"
#include "imgui/imgui_impl_win32.h"
#include "console.h"

namespace Hooks
{
    VMT* entityVMT;
    VMT* panelVMT;
    VMT* particleMgrVMT;
    VMT* particleCollectionVMT;
    VMT* clientVMT;
    VMT* cameraVMT;
}

void Hooks::Init()
{
    g_console.log("-=-=-=-=-=-=-=-=-=-= Hooks -=-=-=-=-=-=-=-=-=-=-");


    if (!D3D9::CreateDummyDevice(D3D9::vTable, sizeof(D3D9::vTable))) return;
    D3D9::EndSceneAddress = (mem_t)D3D9::vTable[42];

    D3D9::oEndScene = (D3D9::EndScene_t)Memory::In::Hook::TrampolineHook((byte_t*)D3D9::EndSceneAddress, (byte_t*)D3D9::hkEndScene, 15);
    g_console.log("%-30s: %i", "EndScene", 15);
}

void Hooks::Shutdown()
{
	if (!D3D9::EndSceneAddress) return;
		Memory::In::Hook::Restore(D3D9::EndSceneAddress);
}