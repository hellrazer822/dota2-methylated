#pragma once

#include "interfaces.h"
#include "definitions.h"
#include "Hooks.h"

void* FrameStageNotify(CSource2Client* thisptr, ClientFrameStage_t stage);
