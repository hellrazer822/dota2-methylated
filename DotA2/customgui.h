#pragma once

#define IMGUI_DEFINE_MATH_OPERATORS
#define IM_USE using namespace ImGui;

#include "imgui/imgui.h"
#include "imgui/imgui_internal.h"
#include "imgui/imstb_textedit.h"

#include <string>

namespace CGUI
{
	void		DrawLogo(ImVec2 center, float size = 30, bool pulse = false, float speeed = 0.35f, int alpha = 255);
	bool Tab(const char* label, bool selected);
}