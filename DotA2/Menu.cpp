#define IMGUI_DEFINE_MATH_OPERATORS
#include "Menu.h"
#include "console.h"
#include <WinUser.h>
#include "user.h"

WNDPROC WndProc_o = nullptr;
LRESULT CALLBACK hWndProc1(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

void Menu::Init(IDirect3DDevice9* pDevice)
{
    HWND hWindow = FindWindowA(NULL, "Dota 2");
    WndProc_o = (WNDPROC)SetWindowLongPtr(hWindow, GWLP_WNDPROC, (LONG_PTR)hWndProc1);

    ImGui::CreateContext();

    D3DXCreateTextureFromFileA(pDevice, "C:\\Methylated\\avatar.methylated", &Menu::Get().avatar);

    CreateStyle();
    InitFonts();

    ImGui_ImplDX9_Init(g_Render.pDevice);
    ImGui_ImplWin32_Init(hWindow);
}

void Menu::Render()
{
	if (!_visible)
		return;

	ImGui::SetNextWindowSize({200,180});

	if (ImGui::Begin("##main_menu",&Menu::Get()._visible, menu_flags))
	{
        menu_draw = ImGui::GetWindowDrawList();
        menu_pos = ImGui::GetWindowPos();

        // Logo Space
        CGUI::DrawLogo(menu_pos + ImVec2(67, 30), 20, true);
        ImGui::PushFont(Font::main_middle);
        menu_draw->AddText(menu_pos + ImVec2(97,15),ImColor(215, 215, 215),"Bruh\nExperience");
        ImGui::PopFont();
        ImGui::Dummy({ 200,50 });
        // -- Logo Space


        // Tabs Space
        ImGui::PushFont(Font::main_middle);

        if (CGUI::Tab("Information", _selectedtab == 1))
            if (_selectedtab != 1) _selectedtab = 1, _selectedpos = ImGui::GetCursorPos(), _selectedtabsize = 30; else _selectedtab = 0;

        if (CGUI::Tab("World", _selectedtab == 2))
            if (_selectedtab != 2) _selectedtab = 2, _selectedpos = ImGui::GetCursorPos(), _selectedtabsize = 30; else _selectedtab = 0;

        ImGui::PopFont();
        // --Tabs Space



        // User Space
        ImGui::PushFont(Font::main_big);

        menu_draw->AddImageRounded(avatar, { menu_pos.x + 10, menu_pos.y + 140 }, { menu_pos.x + 40, menu_pos.y + 170 }, ImVec2(0, 0), ImVec2(1, 1), ImColor(255, 255, 255), 8.f);
        ImGui::SetCursorPos({ 50, 146 }),
        ImGui::Text(username.c_str());

        ImGui::PopFont();
        // --User Space
		ImGui::End();
	}

    if (_selectedtab != 0)
    {
        ImGui::SetNextWindowPos({ menu_pos.x + 209, menu_pos.y + _selectedpos.y - 44});
        ImGui::SetNextWindowSize({ 200, 20 + _selectedtabsize });
        if (ImGui::Begin("##secondary_menu", &Menu::Get()._visible, menu_flags))
        {
            ImGui::PushFont(Font::main_middle);

            if (_selectedtab == 2)
            {
                ImGui::Checkbox("Unlock Partickles", &Options::World.unlockParticles);
            }

            ImGui::PopFont();

            ImGui::End();
        }
    }
}

void Menu::NewFrame() 
{
	ImGui_ImplDX9_NewFrame();
	ImGui_ImplWin32_NewFrame();
	ImGui::NewFrame();
}

void Menu::RenderFrame() 
{
	ImGui::Render();
	ImGui_ImplDX9_RenderDrawData(ImGui::GetDrawData());
}

void Menu::Shutdown()
{
	ImGui_ImplDX9_Shutdown();
	ImGui_ImplWin32_Shutdown();
	ImGui::DestroyContext();
}

void Menu::Switch()
{
	_visible = !_visible;
    g_console.log("Menu State Switched [%i]",_visible);
}

bool Menu::IsVisible()
{
	return _visible;
}

void Menu::CreateStyle()
{
    ImGuiStyle* style = &ImGui::GetStyle();
    ImVec4* colors = style->Colors;

    colors[ImGuiCol_Text] = ImVec4(1.00f, 1.00f, 1.00f, 1.00f);
    colors[ImGuiCol_TextDisabled] = ImVec4(0.50f, 0.50f, 0.50f, 1.00f);
    colors[ImGuiCol_WindowBg] = ImVec4(0.06f, 0.06f, 0.06f, 0.94f);
    colors[ImGuiCol_ChildBg] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
    colors[ImGuiCol_PopupBg] = ImVec4(0.08f, 0.08f, 0.08f, 0.94f);
    colors[ImGuiCol_Border] = ImVec4(0.43f, 0.43f, 0.50f, 0.50f);
    colors[ImGuiCol_BorderShadow] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
    colors[ImGuiCol_FrameBg] = ImVec4(0.16f, 0.29f, 0.48f, 0.54f);
    colors[ImGuiCol_FrameBgHovered] = ImVec4(0.26f, 0.59f, 0.98f, 0.40f);
    colors[ImGuiCol_FrameBgActive] = ImVec4(0.26f, 0.59f, 0.98f, 0.67f);
    colors[ImGuiCol_TitleBg] = ImVec4(0.04f, 0.04f, 0.04f, 1.00f);
    colors[ImGuiCol_TitleBgActive] = ImVec4(0.16f, 0.29f, 0.48f, 1.00f);
    colors[ImGuiCol_TitleBgCollapsed] = ImVec4(0.00f, 0.00f, 0.00f, 0.51f);
    colors[ImGuiCol_MenuBarBg] = ImVec4(0.14f, 0.14f, 0.14f, 1.00f);
    colors[ImGuiCol_ScrollbarBg] = ImVec4(0.02f, 0.02f, 0.02f, 0.53f);
    colors[ImGuiCol_ScrollbarGrab] = ImVec4(0.31f, 0.31f, 0.31f, 1.00f);
    colors[ImGuiCol_ScrollbarGrabHovered] = ImVec4(0.41f, 0.41f, 0.41f, 1.00f);
    colors[ImGuiCol_ScrollbarGrabActive] = ImVec4(0.51f, 0.51f, 0.51f, 1.00f);
    colors[ImGuiCol_CheckMark] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
    colors[ImGuiCol_SliderGrab] = ImVec4(0.24f, 0.52f, 0.88f, 1.00f);
    colors[ImGuiCol_SliderGrabActive] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
    colors[ImGuiCol_Button] = ImVec4(0.26f, 0.59f, 0.98f, 0.40f);
    colors[ImGuiCol_ButtonHovered] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
    colors[ImGuiCol_ButtonActive] = ImVec4(0.06f, 0.53f, 0.98f, 1.00f);
    colors[ImGuiCol_Header] = ImVec4(0.26f, 0.59f, 0.98f, 0.31f);
    colors[ImGuiCol_HeaderHovered] = ImVec4(0.26f, 0.59f, 0.98f, 0.80f);
    colors[ImGuiCol_HeaderActive] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
    colors[ImGuiCol_Separator] = colors[ImGuiCol_Border];
    colors[ImGuiCol_SeparatorHovered] = ImVec4(0.10f, 0.40f, 0.75f, 0.78f);
    colors[ImGuiCol_SeparatorActive] = ImVec4(0.10f, 0.40f, 0.75f, 1.00f);
    colors[ImGuiCol_ResizeGrip] = ImVec4(0.26f, 0.59f, 0.98f, 0.25f);
    colors[ImGuiCol_ResizeGripHovered] = ImVec4(0.26f, 0.59f, 0.98f, 0.67f);
    colors[ImGuiCol_ResizeGripActive] = ImVec4(0.26f, 0.59f, 0.98f, 0.95f);
    colors[ImGuiCol_Tab] = ImLerp(colors[ImGuiCol_Header], colors[ImGuiCol_TitleBgActive], 0.80f);
    colors[ImGuiCol_TabHovered] = colors[ImGuiCol_HeaderHovered];
    colors[ImGuiCol_TabActive] = ImLerp(colors[ImGuiCol_HeaderActive], colors[ImGuiCol_TitleBgActive], 0.60f);
    colors[ImGuiCol_TabUnfocused] = ImLerp(colors[ImGuiCol_Tab], colors[ImGuiCol_TitleBg], 0.80f);
    colors[ImGuiCol_TabUnfocusedActive] = ImLerp(colors[ImGuiCol_TabActive], colors[ImGuiCol_TitleBg], 0.40f);
    colors[ImGuiCol_PlotLines] = ImVec4(0.61f, 0.61f, 0.61f, 1.00f);
    colors[ImGuiCol_PlotLinesHovered] = ImVec4(1.00f, 0.43f, 0.35f, 1.00f);
    colors[ImGuiCol_PlotHistogram] = ImVec4(0.90f, 0.70f, 0.00f, 1.00f);
    colors[ImGuiCol_PlotHistogramHovered] = ImVec4(1.00f, 0.60f, 0.00f, 1.00f);
    colors[ImGuiCol_TextSelectedBg] = ImVec4(0.26f, 0.59f, 0.98f, 0.35f);
    colors[ImGuiCol_DragDropTarget] = ImVec4(1.00f, 1.00f, 0.00f, 0.90f);
    colors[ImGuiCol_NavHighlight] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
    colors[ImGuiCol_NavWindowingHighlight] = ImVec4(1.00f, 1.00f, 1.00f, 0.70f);
    colors[ImGuiCol_NavWindowingDimBg] = ImVec4(0.80f, 0.80f, 0.80f, 0.20f);
    colors[ImGuiCol_ModalWindowDimBg] = ImVec4(0.80f, 0.80f, 0.80f, 0.35f);

    style->WindowPadding = ImVec2(10, 10);
    style->WindowRounding = 6;
    style->WindowBorderSize = 0;
    style->AntiAliasedLines = 1.f;
    style->AntiAliasedFill = 1.f;
}

void Menu::InitFonts()
{
    ImGuiIO& io = ImGui::GetIO(); (void)io;

    Font::main = ImGui::GetIO().Fonts->AddFontFromMemoryCompressedTTF(sfpro_compressed_data, sfpro_compressed_size, 12, NULL, io.Fonts->GetGlyphRangesCyrillic());
    Font::main_middle = ImGui::GetIO().Fonts->AddFontFromMemoryCompressedTTF(sfpro_compressed_data, sfpro_compressed_size, 14, NULL, io.Fonts->GetGlyphRangesCyrillic());
    Font::main_big = ImGui::GetIO().Fonts->AddFontFromMemoryCompressedTTF(sfpro_compressed_data, sfpro_compressed_size, 16, NULL, io.Fonts->GetGlyphRangesCyrillic());
}

LRESULT CALLBACK hWndProc1(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{

    if (ImGui::GetCurrentContext() == NULL)
        return 0;

    if (Menu::Get().IsVisible())
    {
        ImGuiIO& io = ImGui::GetIO();
        switch (uMsg)
        {
        case WM_LBUTTONDOWN: case WM_LBUTTONDBLCLK:
        case WM_RBUTTONDOWN: case WM_RBUTTONDBLCLK:
        case WM_MBUTTONDOWN: case WM_MBUTTONDBLCLK:
        {
            int button = 0;
            if (uMsg == WM_LBUTTONDOWN || uMsg == WM_LBUTTONDBLCLK) button = 0;
            if (uMsg == WM_RBUTTONDOWN || uMsg == WM_RBUTTONDBLCLK) button = 1;
            if (uMsg == WM_MBUTTONDOWN || uMsg == WM_MBUTTONDBLCLK) button = 2;
            if (!ImGui::IsAnyMouseDown() && ::GetCapture() == NULL)
                ::SetCapture(hwnd);
            io.MouseDown[button] = true;
            return 1;
        }
        case WM_LBUTTONUP:
        case WM_RBUTTONUP:
        case WM_MBUTTONUP:
        {
            int button = 0;
            if (uMsg == WM_LBUTTONUP) button = 0;
            if (uMsg == WM_RBUTTONUP) button = 1;
            if (uMsg == WM_MBUTTONUP) button = 2;
            io.MouseDown[button] = false;
            if (!ImGui::IsAnyMouseDown() && ::GetCapture() == hwnd)
                ::ReleaseCapture();
            return 1;
        }
        case WM_MOUSEWHEEL:
            io.MouseWheel += GET_WHEEL_DELTA_WPARAM(wParam) > 0 ? +1.0f : -1.0f;
            return 1;
        case WM_MOUSEHWHEEL:
            io.MouseWheelH += GET_WHEEL_DELTA_WPARAM(wParam) > 0 ? +1.0f : -1.0f;
            return 1;
        case WM_MOUSEMOVE:
            io.MousePos.x = (signed short)(lParam);
            io.MousePos.y = (signed short)(lParam >> 16);
            return 1;
        case WM_KEYDOWN:
        case WM_SYSKEYDOWN:
            if (wParam < 256)
                io.KeysDown[wParam] = 1;
            return 1;
        case WM_KEYUP:
        case WM_SYSKEYUP:
            if (wParam < 256)
                io.KeysDown[wParam] = 0;
            return 1;
        case WM_CHAR:
            // You can also use ToAscii()+GetKeyboardState() to retrieve characters.
            if (wParam > 0 && wParam < 0x10000)
                io.AddInputCharacter((unsigned short)wParam);
            return 1;
        }
    }

    return CallWindowProc(WndProc_o, hwnd, uMsg, wParam, lParam);
}

namespace Font
{
    ImFont* main;
    ImFont* main_middle;
    ImFont* main_big;
}
